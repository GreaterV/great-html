// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

const path = require(`path`);

module.exports = {
  // The root directory that Jest should scan for tests and modules within
  rootDir: path.join(__dirname, `lib`),

  // The test environment that will be used for testing
  testEnvironment: `node`,
};
