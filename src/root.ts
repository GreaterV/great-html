import Children from "./children";


export default class Root {
    private children : Children;

    public constructor(children : Children) {
        this.children = children;
    }

    public get Children() : Children {
        return this.children;
    }

    public toString() : string {
        return this.Children.toString(false);
    }
}
