import Attribute from "./attribute";
import StringAttribute from "./string-attribute";


it(`Should converts to string`, () => {
    const attribute = new StringAttribute(`my-class`);

    expect(attribute).toBeInstanceOf(StringAttribute);
    expect(attribute).toBeInstanceOf(Attribute);
    expect(attribute.Value).toBe(`my-class`);
    expect(attribute.toString()).toBe(`my-class`);
});
