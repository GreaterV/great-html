import Attributes from "./attributes";
import StringAttribute from "./string-attribute";


it(`Should be converts to string`, () => {
    const attributes = new Attributes;

    attributes.set(`class`, new StringAttribute(`my-class`));
    attributes.set(`style`, new StringAttribute(`my-style`));

    expect(attributes).toBeInstanceOf(Attributes);
    expect(attributes.size).toBe(2);
    expect(attributes.toString()).toBe(` class="my-class" style="my-style"`);
});
