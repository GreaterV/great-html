import Attributes from "./attributes";
import StringAttribute from "./string-attribute";
import VoidElement from "./void-element";


it(`Should be creatable without attributes`, () => {
    const element = new VoidElement(`meta`);

    expect(element).toBeInstanceOf(VoidElement);
    expect(element.Name).toBe(`meta`);
    expect(element.Attributes.size).toBe(0);
});
it(`Should returns attributes passed in constructor`, () => {
    const attributes = new Attributes;

    attributes.set(`class`, new StringAttribute(`my-class`));

    const element = new VoidElement(`meta`, attributes);

    expect(element).toBeInstanceOf(VoidElement);
    expect(element.Name).toBe(`meta`);
    expect(element.Attributes).toBe(attributes);
    expect(element.toString()).toBe(`<meta class="my-class">`);
});
