import NamedElement from "./named-element";
import Attributes from "./attributes";
import Root from "./root";
import Children from "./children";
import DeclarationElement from "./declaration-element";


export default abstract class BasicElement extends NamedElement {
    private attributes : Attributes;

    public constructor(name : string, attributes : Attributes = new Attributes) {
        super(name);

        this.attributes = attributes;
    }

    public get Attributes() : Attributes {
        return this.attributes;
    }
    public get Html5() : Root {
        return new Root(new Children(
            new DeclarationElement,
            this,
        ));
    }

    public toString() : string {
        return `<${this.Name}${this.Attributes.toString()}>`;
    }
}
