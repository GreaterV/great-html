import NamedElement from "./named-element";


export default class DeclarationElement extends NamedElement {
    private content : Array<string>;

    public constructor(name : string = `DOCTYPE`, content : Array<string> = [ `html` ]) {
        super(name);

        this.content = content;
    }

    public get Content() : Array<string> {
        return this.Content;
    }

    public toString() : string {
        const content = this.content.map(x => ` ${x}`).join(``);

        return `<!${this.Name}${content}>`;
    }
}
