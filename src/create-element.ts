import Element from "./element";
import BasicElement from "./basic-element";
import Attributes from "./attributes";
import StringAttribute from "./string-attribute";
import VoidElement from "./void-element";
import Children from "./children";
import StandardElement from "./standard-element";
import TextElement from "./text-element";


/**
 * @see https://html.spec.whatwg.org/multipage/syntax.html#elements-2
 */
const VOID_ELEMENTS = [
    `area`,
    `base`,
    `br`,
    `col`,
    `embed`,
    `hr`,
    `img`,
    `input`,
    `link`,
    `meta`,
    `param`,
    `source`,
    `track`,
    `wbr`,
];


export default function createElement(name : string, attributes? : { [key: string] : string }, ...children : Array<boolean | number | string | Element>) : BasicElement {
    const elementAttributes = new Attributes;

    if (attributes) {
        for (const [ key, value ] of Object.entries(attributes)) {
            elementAttributes.set(key, new StringAttribute(value));
        }
    }

    if (children.length === 0) {
        return new VoidElement(name, elementAttributes);
    }
    else {
        if (VOID_ELEMENTS.includes(name)) {
            throw new Error(`Element ${name} should not contain children.`);
        }
    }

    const elementChildren = new Children(...children.map(child => child instanceof Element
        ? child
        : new TextElement(child.toString())
    ));

    return new StandardElement(name, elementAttributes, elementChildren);
}
