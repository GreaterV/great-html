import Element from "./element";
import TextElement from "./text-element";


const INDENTATION = `\t`;
const LINE_BREAK  = `\n`;


export default class Children extends Array<Element> {
    public toString(tab : boolean = true) : string {
        // keep the text surrounded by tags: <tag></tag>text<tag></tag>
        let content = ``;
        let isLastText  = false;
        let isFirst = true;

        for (const child of this) {
            const isText = child instanceof TextElement;

            if (!isText && !isLastText) {
                if (!isFirst || tab) {
                    content += LINE_BREAK;
                }
            }

            content += child.toString();

            isLastText = isText;
            isFirst = false;
        }

        if (tab) {
            content = content.replace(new RegExp(LINE_BREAK, `g`), LINE_BREAK + INDENTATION);
        }

        if (!isLastText) {
            content += LINE_BREAK;
        }

        return content;
    }
}
