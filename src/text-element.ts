import Element from "./element";


export default class TextElement extends Element {
    private string : string;

    public constructor(string : string) {
        super();

        this.string = string;
    }

    public get String() : string {
        return this.string;
    }

    public toString() : string {
        return this.String;
    }
}
