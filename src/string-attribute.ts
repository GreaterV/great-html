import Attribute from "./attribute";


export default class StringAttribute extends Attribute {
    private value : string;

    public constructor(value : string) {
        super();

        this.value = value;
    }

    public get Value() : string {
        return this.value;
    }

    public toString() : string {
        return this.Value;
    }
}
