export default abstract class Attribute {
    public abstract get Value() : any;
    public abstract toString() : string;
}
