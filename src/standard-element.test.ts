import TextElement from "./text-element";
import VoidElement from "./void-element";
import StandardElement from "./standard-element";
import Attributes from "./attributes";
import Children from "./children";


it(`Should be creatable without attributes and children`, () => {
    const element = new StandardElement(`div`);

    expect(element).toBeInstanceOf(StandardElement);
    expect(element.Name).toBe(`div`);
    expect(element.Attributes.size).toBe(0);
    expect(element.Children.length).toBe(0);
});
it(`Should converts to string`, () => {
    const element = new StandardElement(`div`, new Attributes, new Children(
        new TextElement(`text 1`),
        new TextElement(`text 2`),
        new VoidElement(`br`),
        new VoidElement(`br`),
        new VoidElement(`br`),
        new TextElement(`text 3`),
        new VoidElement(`br`),
    ));

    expect(element).toBeInstanceOf(StandardElement);
    expect(element.Name).toBe(`div`);
    expect(element.Attributes.size).toBe(0);
    expect(element.Children.length).toBe(7);
    expect(element.toString()).toBe(
        `<div>text 1text 2<br>\n` +
        `\t<br>\n` +
        `\t<br>text 3<br>\n` +
        `</div>`
    );
});
