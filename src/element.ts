export default abstract class Element {
    public abstract toString() : string;
}
