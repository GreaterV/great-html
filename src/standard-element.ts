import BasicElement from "./basic-element";
import Attributes from "./attributes";
import Children from "./children";


export default class StandardElement extends BasicElement {
    private children : Children;

    public constructor(name : string, attributes : Attributes = new Attributes, children : Children = new Children) {
        super(name, attributes);

        this.children = children;
    }

    public get Children() : Children {
        return this.children;
    }

    public toString() : string {
        return `${super.toString()}${this.Children.toString()}</${this.Name}>`;
    }
}
