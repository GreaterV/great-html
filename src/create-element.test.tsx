import createElement from "./create-element";
import BasicElement from "./basic-element";


const html = { createElement };


it(`Should converts into correct string`, () => {
    /* @jsx createElement  */
    const element : BasicElement = (
        <html lang="en">
            <head>
                <meta charset="utf-8" />
                <title>html</title>
            </head>
            <body>
                <div class="my-class">
                    <span style="my-style">hello</span>
                </div>
            </body>
        </html>
    );

    expect(element.Html5.toString()).toBe(
        `<!DOCTYPE html>\n` +
        `<html lang="en">\n` +
        `\t<head>\n` +
        `\t\t<meta charset="utf-8">\n` +
        `\t\t<title>html</title>\n` +
        `\t</head>\n` +
        `\t<body>\n` +
        `\t\t<div class="my-class">\n` +
        `\t\t\t<span style="my-style">hello</span>\n` +
        `\t\t</div>\n` +
        `\t</body>\n` +
        `</html>\n`
    );
});
