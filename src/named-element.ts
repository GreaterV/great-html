import Element from "./element";


export default abstract class NamedElement extends Element {
    private name : string;

    public constructor(name : string) {
        super();

        this.name = name;
    }

    public get Name() : string {
        return this.name;
    }
}
