import Attribute from "./attribute";


export default class Attributes extends Map<string, Attribute> {
    public get Entries() : Array<[ string, Attribute ]> {
        return [ ...this.entries() ];
    }
    public toString() : string {
        return this.Entries
            .map(([ name, attribute ]) =>
                ` ${name}=${JSON.stringify(attribute.toString())}`
            )
            .join(``)
            ;
    }
}
